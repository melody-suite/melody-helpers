<?php

namespace Orchestra\Helpers\Arr;

use Orchestra\Exceptions\ArrIncompatibleTypeException;
use Orchestra\Exceptions\EmptyKeyException;

class Arr
{
   const ORIENTATION_DESC = 'desc';
   const ORIENTATION_ASC = 'asc';

   private function __construct()
   {
   }

   public static function from(array $arr)
   {
      return ArrBuilder::from($arr);
   }

   public static function wrap($data)
   {
      if (empty($data)) {
         return [];
      }

      if (is_array($data)) {
         return $data;
      }

      return [$data];
   }

   public static function push(array &$arr, $element)
   {
      $arr[] = $element;
   }

   public static function pop(array &$arr)
   {
      return array_pop($arr);
   }

   public static function first(array $arr)
   {
      return $arr[0];
   }

   public static function last(array $arr)
   {
      return end($arr);
   }

   public static function set(array &$arr, string $key, $value)
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array key not provided.");
      }

      $keys = explode('.', $key);

      while (count($keys) > 1) {
         $key = array_shift($keys);

         if (!isset($arr[$key]) || !is_array($arr[$key])) {
            $arr[$key] = [];
         }

         $arr = &$arr[$key];
      }

      $arr[array_shift($keys)] = $value;
   }

   public static function get(array &$arr, string $key, $default = null)
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array key not provided.");
      }

      $keys = explode('.', $key);

      $key = array_shift($keys);

      while (count($keys) > 0) {

         if (!isset($arr[$key]) || !is_array($arr[$key])) {
            return $default;
         }

         $arr = &$arr[$key];

         $key = array_shift($keys);
      }

      if (isBlank($arr[$key])) {
         return $default;
      }

      return $arr[$key];
   }

   public static function remove(array &$arr, string $key)
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array key not provided.");
      }

      $keys = explode('.', $key);

      $key = array_shift($keys);

      while (count($keys) > 0) {

         if (!isset($arr[$key]) || !is_array($arr[$key])) {
            return;
         }

         $arr = &$arr[$key];

         $key = array_shift($keys);
      }

      unset($arr[$key]);
   }

   public static function removeFirst(array &$arr)
   {
      unset($arr[0]);
   }

   public static function removeLast(array &$arr)
   {
      self::pop($arr);
   }

   public static function keys(array $arr)
   {
      return array_keys($arr);
   }

   public static function values(array $arr)
   {
      return array_values($arr);
   }

   public static function merge(array &$arr, array $target)
   {
      if (is_array($target)) {
         $arr = array_merge_recursive($arr, $target);

         return;
      }

      if ($target instanceof ArrBuilder) {
         $arr = array_merge_recursive($arr, $target->all());

         return;
      }

      throw new ArrIncompatibleTypeException(gettype($target) . " is incompatible with Arr Helper.");
   }

   public static function combine(array &$arr, array $target)
   {
      if (is_array($target)) {
         $arr = array_combine($arr, $target);

         return;
      }

      if ($target instanceof ArrBuilder) {
         $arr = array_combine($arr, $target->all());

         return;
      }

      throw new ArrIncompatibleTypeException(gettype($target) . " is incompatible with Arr Helper.");
   }

   public static function prepend(array &$arr, $value, string $key = null)
   {
      if (isBlank($key)) {
         array_unshift($arr, $value);

         return;
      }

      $arr = [$key => $value] + $arr;
   }

   public static function exists(array $arr, string $key): bool
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array {{exists}} key not provided.");
      }

      $keys = explode('.', $key);

      while (count($keys) > 0) {
         $key = array_shift($keys);

         if (!isset($arr[$key])) {
            return false;
         }
      }

      return true;
   }

   public static function sortOrderAsc(array &$arr)
   {
      asort($arr);
   }

   public static function sortOrderDesc(array &$arr)
   {
      arsort($arr);
   }

   public static function sortByOrderAsc(array &$arr, string $key)
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array {{sortByOrderAsc}} key not provided.");
      }

      usort($arr, function ($a, $b) use ($key) {
         return $a[$key] <=> $b[$key];
      });
   }

   public static function sortByOrderDesc(array &$arr, string $key)
   {
      if (isBlank($key)) {
         throw new EmptyKeyException("Array {{sortByOrderDesc}} key not provided.");
      }

      usort($arr, function ($a, $b) use ($key) {
         return $b[$key] <=> $a[$key];
      });
   }

   public static function unique(array &$arr)
   {
      $arr = array_unique($arr);
   }

   public static function join(array $arr, $glue)
   {
      return join($glue, $arr);
   }
}
