<?php

namespace Orchestra\Helpers\Arr;

use Orchestra\Helpers\Collection\Collection;

class ArrBuilder
{
   protected $items;

   private function __construct(array $items)
   {
      $this->items = $items;
   }

   public static function from(array $items)
   {
      return new ArrBuilder($items);
   }

   public function push($element)
   {
      Arr::push($this->items, $element);

      return $this;
   }

   public function pop()
   {
      return Arr::pop($this->items);
   }

   public function first()
   {
      return Arr::first($this->items);
   }

   public function last()
   {
      return Arr::last($this->items);
   }

   public function set(string $key, $value)
   {
      Arr::set($this->items, $key, $value);

      return $this;
   }

   public function get(string $key, $default = null)
   {
      return Arr::get($this->items, $key, $default);
   }

   public function remove(string $key)
   {
      Arr::remove($this->items, $key);
   }

   public function removeFirst()
   {
      Arr::removeFirst($this->items);
   }

   public function removeLast()
   {
      Arr::removeLast($this->items);
   }

   public function keys()
   {
      return Arr::keys($this->items);
   }

   public function values()
   {
      return Arr::values($this->items);
   }

   public function merge(array $target)
   {
      Arr::merge($this->items, $target);

      return $this;
   }

   public function combine(array $target)
   {
      Arr::combine($this->items, $target);

      return $this;
   }

   public function prepend($value, string $key = null)
   {
      Arr::prepend($this->items, $value, $key);

      return $this;
   }

   public function exists(string $key): bool
   {
      return Arr::exists($this->items, $key);
   }

   public function sortOrderAsc()
   {
      Arr::sortOrderAsc($this->items);

      return $this;
   }

   public function join($glue)
   {
      return Arr::join($this->items, $glue);
   }

   public function sortOrderDesc()
   {
      Arr::sortOrderDesc($this->items);

      return $this;
   }

   public function sortByOrderAsc(string $key)
   {
      Arr::sortByOrderAsc($this->items, $key);

      return $this;
   }

   public function sortByOrderDesc(string $key)
   {
      Arr::sortByOrderDesc($this->items, $key);

      return $this;
   }

   public function all()
   {
      return $this->items;
   }

   public function toCollection()
   {
      return Collection::from($this->items);
   }

   public function clone()
   {
      return new ArrBuilder($this->items);
   }

   public function unique()
   {
      array_unique($this->items);

      return $this;
   }
}
