<?php

namespace Orchestra\Helpers\Collection;

use Orchestra\Exceptions\EmptyCallbackException;
use Orchestra\Helpers\Arr\Arr;
use Orchestra\Helpers\Arr\ArrBuilder;

class Collection extends ArrBuilder
{
   private function __construct(array $items)
   {
      $this->items = $items;
   }

   public static function from(array $items)
   {
      return new Collection($items);
   }

   public function empty(string $key)
   {
      return empty($this->get($key));
   }

   public function blank(string $key)
   {
      return isBlank($this->get($key));
   }

   public function filled(string $key)
   {
      return isFilled($this->get($key));
   }

   public function filter(\Closure $callback = null)
   {
      $this->items = !empty($callback) ? array_filter($this->items, $callback, ARRAY_FILTER_USE_BOTH) : array_filter($this->items);

      return $this;
   }

   public function reject(\Closure $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{reject}} callback not provided");
      }

      $this->items = array_filter($this->items, function ($item, $key) use ($callback) {

         return !$callback($item, $key);
      }, ARRAY_FILTER_USE_BOTH);

      return $this;
   }

   public function each(\Closure $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{each}} callback not provided");
      }

      array_walk($this->items, $callback);

      return $this;
   }

   public function map(\Closure $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{map}} callback not provided");
      }

      return array_map($callback, $this->items, $this->keys());
   }

   public function mapWithKeys(callable $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{mapWithKeys}} callback not provided");
      }

      $return = [];

      $this->each(function ($item, $key) use (&$return, $callback) {

         $this->from($callback($item, $key))
            ->each(function ($item, $key) use (&$return) {

               Arr::set($return, $key, $item);
            });
      });

      return $return;
   }

   public function transform(\Closure $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{transform}} callback not provided");
      }

      $this->items = $this->map($callback);

      return $this;
   }

   public function transformWithKeys(\Closure $callback)
   {
      if (empty($callback)) {
         throw new EmptyCallbackException("Collection {{transformWithKeys}} callback not provided");
      }

      $this->items = $this->mapWithKeys($callback);

      return $this;
   }

   public function join($glue)
   {
      return Arr::join($this->items, $glue);
   }

   public function all()
   {
      return $this->items;
   }

   public function clone()
   {
      return new Collection($this->items);
   }
}
