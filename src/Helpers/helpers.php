<?php

use Orchestra\Helpers\Arr\Arr;

function isFilled($value): bool
{
   if (is_null($value) || $value == "" || !isset($value)) {
      return false;
   }

   return true;
}

function isBlank($value): bool
{
   return !isFilled($value);
}

function env($key, $default = null)
{
   $env = parse_ini_file('.env');

   return Arr::get($env, $key, $default);
}
