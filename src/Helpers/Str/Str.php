<?php

namespace Orchestra\Helpers\Str;

class Str
{
   public static function from(string $str)
   {
      return StrBuilder::from($str);
   }

   public static function replace(string $str, string $from, string $to)
   {
      return str_replace($from, $to, $str);
   }

   public static function replaceFirst(string $str, string $from, string $to)
   {
      return preg_replace("/$from/", $to, $str, 1);
   }

   public static function replaceLast(string $str, string $from, string $to)
   {
      $pos = strrpos($str, $from);

      if ($pos !== false) {
         return substr_replace($str, $to, $pos, strlen($from));
      }

      return $str;
   }

   public static function toLower(string $str)
   {
      return strtolower($str);
   }

   public static function toUpper(string $str)
   {
      return strtoupper($str);
   }

   public static function indexOfFirst(string $str, string $from)
   {
      return stripos($str, $from);
   }

   public static function indexOfLast(string $str, string $from)
   {
      return strrpos($str, $from);
   }

   public static function indexOfAll(string $str, string $from)
   {
      $offset = 0;

      $allpos = array();

      while (($pos = strpos($str, $from, $offset)) !== false) {
         $offset   = $pos + 1;

         $allpos[] = $pos;
      }

      return $allpos;
   }

   public static function split(string $str, string $delimiter)
   {
      return explode($delimiter, $str);
   }

   public static function toCamel(string $str, string $delimiter = "_", bool $capitalizeFirstCharacter)
   {
      $str = str_replace($delimiter, '', ucwords($str, $delimiter));

      if (!$capitalizeFirstCharacter) {
         $str = lcfirst($str);
      }

      return $str;
   }

   public static function toSnake(string $str, string $delimiter = "_")
   {
      return str_replace($delimiter, '_', Str::toLower($str));
   }

   public static function suffix(string $str, string $suffix)
   {
      return rtrim($str, $suffix) . $suffix;
   }

   public static function prefix(string $str, string $prefix)
   {
      return $prefix . rtrim($str, $prefix);
   }

   public static function contains(string $str, string $needle)
   {
      return self::indexOfFirst($str, $needle) >= 0;
   }
}
