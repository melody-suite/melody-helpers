<?php

namespace Orchestra\Helpers\Str;

class StrBuilder
{
   private $str;

   private function __construct(string $str)
   {
      $this->str = $str;
   }

   public static function from(string $str)
   {
      return new StrBuilder($str);
   }

   public function replace(string $from, string $to)
   {
      $this->str = Str::replace($this->str, $from, $to);

      return $this;
   }

   public function replaceFirst(string $from, string $to)
   {
      $this->str = Str::replaceFirst($this->str, $from, $to);

      return $this;
   }

   public function replaceLast(string $from, string $to)
   {
      $this->str = Str::replaceLast($this->str, $from, $to);

      return $this;
   }

   public function toLower()
   {
      $this->str = Str::toLower($this->str);

      return $this;
   }

   public function toUpper()
   {
      $this->str = Str::toUpper($this->str);

      return $this;
   }

   public function indexOfFirst(string $from)
   {
      return Str::indexOfFirst($this->str, $from);
   }

   public function indexOfLast(string $from)
   {
      return Str::indexOfLast($this->str, $from);
   }

   public function indexOfAll(string $from)
   {
      return Str::indexOfAll($this->str, $from);
   }

   public function toCamel(string $delimiter = "_", bool $capitalizeFirstCharacter)
   {
      $this->str = Str::toLower($this->str, $delimiter, $capitalizeFirstCharacter);

      return $this;
   }

   public function toSnake(string $delimiter = "_")
   {
      $this->str = Str::toSnake($this->str, $delimiter);

      return $this;
   }

   public function suffix(string $str, string $suffix)
   {
      $this->str = rtrim($str, $suffix) . $suffix;

      return $this;
   }

   public function prefix(string $str, string $prefix)
   {
      $this->str = $prefix . rtrim($str, $prefix);

      return $this;
   }

   public function contains(string $str, string $needle)
   {
      return Str::contains($str, $needle);
   }

   public function get()
   {
      return $this->str;
   }
}
